const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

function assetFilter(assetFilename) {
  return !(/\.map$/.test(assetFilename)) && !assetFilename.endsWith('beta.js');
}

module.exports = {
  entry: {
    index: path.join(__dirname, 'src', 'index.jsx'),
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].js',
  },
  target: 'web',
  mode: 'production',
  externals: [],
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  performance: {
    hints: 'warning',
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
    assetFilter,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [{ 
          loader: 'babel-loader',
        }],
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [{ loader: 'json-loader' }],

      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'less-loader',
        }],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [{ 
          loader: 'url-loader',
        }],
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10000,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif|ico)$/i,
        include: path.resolve(__dirname, 'src'),
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ],
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 8080,
    http2: true,
    host: "0.0.0.0"
  },
  plugins: [
    new HtmlWebPackPlugin({
      filename: 'index.html',
      template: path.join(__dirname, 'src', 'index.html'),
      favicon: path.join(__dirname, 'src', 'favicon.ico')
    }),
    new CopyPlugin({
      patterns: [{ from: 'src/robots.txt', to: 'robots.txt' }]
    })
  ],
};
