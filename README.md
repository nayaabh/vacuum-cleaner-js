# RoboVac js

A simple toy vacuum robot that obeys your commands.

![](docs/images/demo.gif)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [RoboVac js](#robovac-js)
  - [Prerequisite](#prerequisite)
  - [Installation](#installation)
  - [Usage](#usage)
    - [Start local server](#start-local-server)
    - [Commands](#commands)
      - [Supported commands](#supported-commands)
    - [Settings](#settings)
  - [Demo](#demo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

## Prerequisite
You would need the below libraries to run the application on your local machine
- Node JS 10.0 or above
  - [Download](https://nodejs.org/en/) 
- Git 2.2 or above
  - [Download](https://git-scm.com/downloads)

## Installation
Copy and paste the below commands in bash to get started.
```node
git clone https://gitlab.com/nayaabh/vacuum-cleaner-js.git
cd vacuum-cleaner-js
npm install
```

## Usage
A simple way to start the application is to use the included webpack dev server to serve the `public/` directory. You can you use any other http server of your choice

### Start local server
```npm
npm start
```

Server will start with the below message
```log
> webpack serve --config webpack.prod.config.js

‼ ｢wds｣: HTTP/2 is currently unsupported for Node 10.0.0 and above, but will be supported once Express supports it
i ｢wds｣: Project is running at https://0.0.0.0:8080/
i ｢wds｣: webpack output is served from /
i ｢wds｣: Content not from webpack is served from P:\workspace\vacuum-cleaner-js\public
i ｢wdm｣: wait until bundle finished: /
```

Open your browser and go to https://localhost:8080 to run the application

### Commands
Command section is visible in the right drawer. Input commands and press `Execute` button to see the robot go!
#### Supported commands
- PLACE x,y,FACE
  - Places the robot on the room floor, where x is position in x-axis and y is the position in y-axis. 
  - Value of x and y starts from 0
  - `FACE` is one of the following 
    - `NORTH`
    - `EAST`
    - `SOUTH`
    - `WEST`
  - E.g. `PLACE 1,2,NORTH`
- MOVE
  - Moves the robot by 1 tile in the direction its facing
- LEFT or RIGHT
  - Rotates the robot Left or right
- REPORT
  - Displays the current location of the robot in a tooltip
  - It can be displayed as a Toast if enabled from the `Settings > Display Report as Toast`

### Settings
The below parameters of the room and the robot can be configured in the settings options. Press on the cog icon to view the settings
![](docs/images/settings-dialog.png)

---
## Demo
You can go to the below link for the demo.

https://nayaabh.gitlab.io/vacuum-cleaner-js