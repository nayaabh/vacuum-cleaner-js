import { InputAdornment } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { makeStyles } from '@material-ui/core/styles'
import Switch from '@material-ui/core/Switch'
import TextField from '@material-ui/core/TextField'
import React from 'react'
import { useSettings } from '../context-components/settings-provider'

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 250,
        },
    },
    toggleSwitches: {
        marginLeft: 10
    }
}));

const MIN_FLOOR_WIDTH = 1
const MIN_FLOOR_HEIGHT = 1
const MAX_FLOOR_WIDTH = 100
const MAX_FLOOR_HEIGHT = 100
const MIN_ROBOT_SIZE = 10
const MAX_ROBOT_SIZE = 100
const MIN_TILE_SIZE = 20
const MAX_TILE_SIZE = 200

export function getValidNumberWithinRange(value, min, max) {
    if (isNaN(value) || value < min) {
        return min
    }
    if (value > max) {
        return max
    }
    return value
}

export default function SettingsDialog({ isOpen, onClose }) {
    const { maxRoomSizeX, setMaxRoomSizeX,
        maxRoomSizeY, setMaxRoomSizeY,
        pixelsPerUnit, setPixlesPerUnit,
        robotSize, setRobotSize,
        animate, setAnimate,
        showToastReport, setShowToastReport } = useSettings()

    const classes = useStyles();
    return (
        <Dialog open={isOpen} onClose={onClose} fullWidth className={classes.root} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Settings</DialogTitle>
            <DialogContent>
                <div>
                    <TextField
                        type="number"
                        label="Room Width"
                        inputProps={{ min: MIN_FLOOR_WIDTH, max: MAX_FLOOR_WIDTH }}
                        value={maxRoomSizeX}
                        variant="outlined"
                        margin="dense"
                        onChange={e => setMaxRoomSizeX(getValidNumberWithinRange(parseInt(e.target.value), MIN_FLOOR_WIDTH, MAX_FLOOR_WIDTH))} />
                    <TextField
                        type="number"
                        label="Room Height"
                        inputProps={{ min: MIN_FLOOR_HEIGHT, max: MAX_FLOOR_HEIGHT }}
                        value={maxRoomSizeY}
                        variant="outlined"
                        margin="dense"
                        onChange={e => setMaxRoomSizeY(getValidNumberWithinRange(parseInt(e.target.value), MIN_FLOOR_HEIGHT, MAX_FLOOR_HEIGHT))} />
                </div>
                <div>
                    <TextField
                        type="number"
                        label="Tile Size"
                        InputProps={{ endAdornment: <InputAdornment position="end">px</InputAdornment> }}
                        inputProps={{ min: MIN_TILE_SIZE, max: MAX_TILE_SIZE }}
                        variant="outlined"
                        margin="dense"
                        value={pixelsPerUnit}
                        onChange={e => setPixlesPerUnit(getValidNumberWithinRange(parseInt(e.target.value), MIN_TILE_SIZE, MAX_TILE_SIZE))} />
                    <TextField
                        type="number"
                        InputProps={{ endAdornment: <InputAdornment position="end">px</InputAdornment> }}
                        inputProps={{ min: MIN_ROBOT_SIZE, max: MAX_ROBOT_SIZE }}
                        value={robotSize}
                        label="Robot Size"
                        variant="outlined"
                        margin="dense"
                        onChange={e => setRobotSize(getValidNumberWithinRange(parseInt(e.target.value), MIN_ROBOT_SIZE, MAX_ROBOT_SIZE))} />
                </div>
                <div className={classes.toggleSwitches}>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={animate}
                                onChange={e => setAnimate(e.target.checked)}
                                name="animate"
                                color="primary"
                            />
                        }
                        label="Animation"
                    />
                </div>
                <div className={classes.toggleSwitches}>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={showToastReport}
                                onChange={e => setShowToastReport(e.target.checked)}
                                name="showToastReport"
                                color="primary"
                            />
                        }
                        label="Show Report as Toast"
                    />
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Done
                </Button>
            </DialogActions>
        </Dialog>
    )
}
