import Snackbar from '@material-ui/core/Snackbar'
import Tooltip from '@material-ui/core/Tooltip'
import React, { useEffect, useRef, useState } from 'react'
import { useSettings } from '../context-components/settings-provider'
import RobotImage from '../images/vacuum-cleaner.svg'
import { DIRECTION_TO_DEGREES } from '../services/robot-command-services'


const generateRobotStyles = (pixelsPerUnit, robotSize) => ({
    marginLeft: pixelsPerUnit / 2 - robotSize / 2,
    marginBottom: pixelsPerUnit / 2 - robotSize / 2,
    height: robotSize,
    width: robotSize
})

const Robot = ({ isReportOpen, positionX, positionY, faceDirection, report, isVisible }) => {
    const { pixelsPerUnit, robotSize, showToastReport } = useSettings()
    const robotRef = useRef()
    const [robotStyles, setRobotStyles] = useState({})

    useEffect(() => {
        setRobotStyles(generateRobotStyles(pixelsPerUnit, robotSize))
    }, [])

    useEffect(() => {
        setRobotStyles(generateRobotStyles(pixelsPerUnit, robotSize))
    }, [pixelsPerUnit, robotSize])

    useEffect(() => {
        robotRef.current.style.left = (positionX * pixelsPerUnit) + 'px'
        robotRef.current.style.bottom = (positionY * pixelsPerUnit) + 'px'
        robotRef.current.style.transform = `rotateZ(${DIRECTION_TO_DEGREES[faceDirection]})`
    }, [positionX, positionY, faceDirection])

    return (
        <div>
            <Tooltip
                open={isReportOpen}
                title={report}
                disableFocusListener
                arrow
            >
                <div className={`robot ${isVisible ? '' : 'hidden'}`} ref={robotRef} style={robotStyles}>
                    <img src={RobotImage} alt={report} />
                </div>
            </Tooltip>
            {showToastReport && <Snackbar open={isReportOpen} autoHideDuration={5000} message={report} />}
        </div>

    )
}

export default Robot