import { AppBar, Drawer, IconButton, Toolbar, Typography } from '@material-ui/core'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Fab from '@material-ui/core/Fab'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Settings } from '@material-ui/icons'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import MenuIcon from '@material-ui/icons/Menu'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import clsx from 'clsx'
import React, { useState, Suspense } from 'react'
import Robot from '../components/robot'
import Room from '../components/room'
import { useCommands } from '../context-components/command-provider'
import { useSettings } from '../context-components/settings-provider'
import RobotImage from '../images/vacuum-cleaner.svg'
import RoboServices, { parseCommandString } from '../services/robot-command-services'
import CommandInput from './command-input'

const SettingsDialog = React.lazy(() => import('./settings-dialog'))

let reportClearTimeOut = null
let commandSequence = []
const DRAWER_WIDTH = 250;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${DRAWER_WIDTH}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginRight: DRAWER_WIDTH,
    },
    title: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start"
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: DRAWER_WIDTH,
        flexShrink: 0,
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginRight: -DRAWER_WIDTH,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginRight: 0,
    },
    drawerFooter: {
        position: "fixed",
        bottom: 10,
        right: 10,
    }
}));

const Stage = () => {

    const { maxRoomSizeX, maxRoomSizeY, animate } = useSettings()
    const { commandInputs } = useCommands()
    const classes = useStyles()
    const theme = useTheme()

    const robotService = new RoboServices(maxRoomSizeX, maxRoomSizeY)

    const [robotXPosition, setRobotXPosition] = useState(robotService.robotData.positionX)
    const [robotYPosition, setRobotYPosition] = useState(robotService.robotData.positionY)
    const [robotFaceDirection, setRobotFaceDirection] = useState(robotService.robotData.faceDirection)
    const [report, setReport] = useState('')

    const [isReportOpen, setIsReportOpen] = useState(false)
    const [isSettingsOpen, setIsSettingsOpen] = useState(false)
    const [drawerOpen, setDrawerOpen] = React.useState(true)

    function executeCommands() {
        robotService.resetRobot()
        if (commandSequence.length) {
            commandSequence.map(clearTimeout)
            commandSequence.length = 0
        }

        parseCommandString(commandInputs)
            .filter(Boolean)
            .forEach((command, index) => {
                commandSequence[index] = setTimeout(() => {
                    robotService.executeCommand(command, { showReport, hideReport })

                    const { positionX, positionY, faceDirection } = robotService.robotData
                    setRobotXPosition(positionX)
                    setRobotYPosition(positionY)
                    setRobotFaceDirection(faceDirection)
                }, animate ? index * 500 : 0)
            })

    }

    function showReport(report) {
        if (reportClearTimeOut !== null) {
            clearTimeout(reportClearTimeOut)
        }
        setReport(report)
        setIsReportOpen(true)
        reportClearTimeOut = setTimeout(() => {
            setIsReportOpen(false)
        }, 5000)
    }

    function hideReport() {
        setIsReportOpen(false)
    }

    function openSettings() {
        setIsSettingsOpen(true)
    }

    function closeSettings() {
        setIsSettingsOpen(false)
    }

    function handleDrawerOpen() {
        setDrawerOpen(true)
    }

    function handleDrawerClose() {
        setDrawerOpen(false)
    }

    return (
        <div className={`stage ${classes.root}`}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: drawerOpen,
                })}
            >
                <Toolbar>
                    <Typography variant="h6" noWrap className={classes.title}>
                        <img src={RobotImage} alt="Logo" height={48} width={48} /><span>RoboVac <sup>js</sup></span>
                    </Typography>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="end"
                        onClick={handleDrawerOpen}
                        className={clsx(drawerOpen && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <main className={clsx(classes.content, {
                [classes.contentShift]: drawerOpen,
            })}>
                <div className={classes.drawerHeader} />
                <Room>
                    <Robot
                        positionX={robotXPosition}
                        positionY={robotYPosition}
                        faceDirection={robotFaceDirection}
                        isVisible={!!robotFaceDirection}
                        isReportOpen={isReportOpen}
                        report={report}
                    />
                </Room>
            </main>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="right"
                open={drawerOpen}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                <CommandInput onExecute={executeCommands} />
                <div className={classes.drawerFooter}>
                    <IconButton onClick={openSettings}>
                        <Settings />
                    </IconButton>
                </div>
            </Drawer>
            <Fab className="execute-floating-button" color="primary" aria-label="execute" onClick={executeCommands}>
                <PlayArrowIcon />
            </Fab>
            <Fab className="settings-button" color="secondary" aria-label="settings" onClick={openSettings}>
                <Settings />
            </Fab>
            <Suspense fallback={<div>Loading...</div>}>
                {isSettingsOpen && <SettingsDialog isOpen={isSettingsOpen} onClose={closeSettings} />}
            </Suspense>
        </div>
    )
}

export default Stage