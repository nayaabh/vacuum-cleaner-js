import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import React, { useEffect, useState } from 'react'
import { useCommands } from '../context-components/command-provider'

const DEMO_COMMANDS = `PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT`

export default function CommandInput({ onExecute }) {
    const [commandInputs, setCommandInputs] = useState(DEMO_COMMANDS);
    const { setCommandInputs: publishCommandContext } = useCommands()

    useEffect(() => {
        publishCommandContext(commandInputs)
    }, [commandInputs])

    return (
        <div className="command-section">
            <TextField
                label="Commands"
                multiline
                size="small"
                rows={20}
                value={commandInputs}
                variant="outlined"
                fullWidth
                margin="dense"
                onChange={element => setCommandInputs(element.target.value)}
            />
            <Button
                variant="contained"
                color="primary"
                onClick={onExecute}
                className="execute-button"
                startIcon={<PlayArrowIcon />}
            >Execute</Button>
        </div>
    )
}
