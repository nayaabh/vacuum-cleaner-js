import React, { useState, useEffect } from 'react'
import { useSettings } from '../context-components/settings-provider'

const getCellDimensions = (ppu) => ({
    height: ppu,
    width: ppu
})

const getRoomDimensions = (ppu, maxX, maxY) => ({
    height: ppu * maxY,
    width: ppu * maxX
})

const Room = ({ children }) => {
    const { maxRoomSizeX, maxRoomSizeY, pixelsPerUnit } = useSettings()
    const [stage, setStage] = useState('')
    const [roomDimensions, setRoomDimensions] = useState({})

    const generateNewStage = () => {
        const cellDimensions = getCellDimensions(pixelsPerUnit)
        return new Array(maxRoomSizeY).fill(new Array(maxRoomSizeX).fill(0)).map((row, indexX, arr) => {
            const isLastRow = indexX === arr.length - 1
            return (<div className={`row ${isLastRow ? 'last-row' : ''}`} key={`R${indexX}`}>
                {row.map((cell, indexY) => (<div className='cell' style={cellDimensions} key={`C${indexX}-${indexY}`}> </div>))}
            </div>)
        })
    }

    useEffect(() => {
        setRoomDimensions(getRoomDimensions(pixelsPerUnit, maxRoomSizeX, maxRoomSizeY))
        setStage(generateNewStage())
        return () => {
            setStage('')
        }
    }, [])
    useEffect(() => {
        setRoomDimensions(getRoomDimensions(pixelsPerUnit, maxRoomSizeX, maxRoomSizeY))
        setStage(generateNewStage())
    }, [maxRoomSizeX, maxRoomSizeY, pixelsPerUnit])
    return (
        <div className="room-floor" style={roomDimensions}>
            {stage}
            {children}
        </div>
    )
}

export default Room