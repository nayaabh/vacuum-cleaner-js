// DIRECTIONS
export const NORTH = 'NORTH'
export const WEST = 'WEST'
export const SOUTH = 'SOUTH'
export const EAST = 'EAST'

// COMMANDS
export const PLACE = 'PLACE'
export const REPORT = 'REPORT'
export const MOVE = 'MOVE'
export const ROTATE = 'ROTATE'
// ROTATIONS
export const LEFT = 'LEFT'
export const RIGHT = 'RIGHT'