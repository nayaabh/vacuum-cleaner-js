import { EAST, LEFT, MOVE, NORTH, PLACE, REPORT, RIGHT, ROTATE, SOUTH, WEST } from "../constants"

const DIRECTIONS = [NORTH, EAST, SOUTH, WEST]
const ROTATIONS = [LEFT, RIGHT]
const noop = () => {}

const getMovementDirection = faceDirection => {
    switch(faceDirection) {
        case NORTH: return { x: 0, y: 1 }
        case EAST: return { x: 1, y: 0 }
        case SOUTH: return { x: 0, y: -1 }
        case WEST: return { x: -1, y: 0 }
        default: return { x: 0, y: 0 }
    }
}

const isWithinBoundry = (position, maxLength) => position >= 0 && position < maxLength

export const DIRECTION_TO_DEGREES = {
    [NORTH]: '0',
    [EAST]: '90deg',
    [SOUTH]: '180deg',
    [WEST]: '-90deg',

}

export const getRotationDirection = (faceDirection, rotateDirection) => {
    if(!ROTATIONS.includes(rotateDirection)) {
        return faceDirection
    }
    switch(faceDirection) {
        case NORTH: return rotateDirection === LEFT ? WEST : EAST
        case EAST: return rotateDirection === LEFT ? NORTH : SOUTH
        case SOUTH: return rotateDirection === LEFT ? EAST : WEST
        case WEST: return rotateDirection === LEFT ? SOUTH : NORTH
        default: return faceDirection
    }
}

export function parseCommandString(input = "") {
    const inputList = input.split('\n')
    return inputList.map(line => line.trim()).map(line => {
        const [rawCommand, rawParams = ''] = line.split(' ')
        const command = rawCommand.trim()
        const params = rawParams.trim()
        switch(command) {
            case PLACE: {
                const [positionX, positionY, faceDirection] = params.split(',')
                if(positionX === undefined || positionY === undefined || !DIRECTIONS.includes(faceDirection)) {
                    return false
                }
                const xCoordinate = parseInt(positionX)
                if (isNaN(xCoordinate)) {
                    return false
                }
                const yCoordinate = parseInt(positionY)
                if (isNaN(yCoordinate)) {
                    return false
                }
                return {
                    commandType: PLACE,
                    params: {
                        positionX: xCoordinate,
                        positionY: yCoordinate,
                        faceDirection
                    }
                }
            }
            case LEFT: return { commandType: ROTATE, params: { rotation: LEFT } }
            case RIGHT: return { commandType: ROTATE, params: { rotation: RIGHT } }
            case MOVE: return { commandType: MOVE }
            case REPORT: return { commandType: REPORT }
            default: return false
        }
    })
}

class RoboServices {
    
    data = {
        positionX: 0,
        positionY: 0,
        faceDirection: null,
    }

    get robotData() {
        return this.data
    }

    get robotReport() {
        const { positionX, positionY, faceDirection } = this.data
        return `${positionX},${positionY},${faceDirection}`
    }

    resetRobot() {
        Object.assign(this.data, {
            positionX: 0,
            positionY: 0,
            faceDirection: null
        })
    }

    constructor(maxX, maxY) {
        this.maxX = maxX
        this.maxY = maxY
    }
    executeCommand({ commandType, params }, { showReport = noop, hideReport = noop } = {}) {
        switch(commandType) {
            case PLACE: {
                const { positionX, positionY, faceDirection } = params
                if (!isWithinBoundry(positionX, this.maxX) || !isWithinBoundry(positionY, this.maxY)) {
                    return this.robotData
                }
                hideReport()
                Object.assign(this.robotData, { positionX, positionY, faceDirection })
                return this.robotData
            }
            case MOVE: {
                if(!this.robotData.faceDirection) {
                    return this.robotData
                }
                const { positionX: oldPositionX, positionY: oldPositionY, faceDirection } = this.robotData
                const {x, y} = getMovementDirection(faceDirection)
                const positionX = oldPositionX + x
                if (!isWithinBoundry(positionX, this.maxX)) {
                    return this.robotData
                }
                const positionY = oldPositionY + y
                if(!isWithinBoundry(positionY, this.maxY)) {
                    return this.robotData
                }
                hideReport()
                this.robotData.positionX = positionX
                this.robotData.positionY = positionY
                
                return this.robotData
            }
            case ROTATE: {
                if(!this.robotData.faceDirection) {
                    return this.robotData
                }
                const { faceDirection: oldFaceDirection } = this.robotData
                const faceDirection = getRotationDirection(oldFaceDirection, params.rotation)
                hideReport()
                this.robotData.faceDirection = faceDirection
                
                return this.robotData
            }
            
            case REPORT: {
                if(!this.robotData.faceDirection) {
                    return this.robotData
                }
                console.log(this.robotReport)
                showReport(this.robotReport)
                return this.robotData
            }

        }
    }
}

export default RoboServices