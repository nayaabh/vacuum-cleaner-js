import React, { createContext, useContext, useState } from "react"

const CommandsContext = createContext();

const CommandProvider = ({ children }) => {
    const [commandInputs, setCommandInputs] = useState('')
    const value = {
        commandInputs, setCommandInputs
    }

    return (
        <CommandsContext.Provider value={value}>
            {children}
        </CommandsContext.Provider>
    );
}


export const useCommands = () => useContext(CommandsContext)

export default CommandProvider;