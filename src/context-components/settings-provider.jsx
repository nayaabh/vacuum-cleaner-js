import React, { createContext, useContext, useState } from "react"

const SettingsContext = createContext();

const SettingsProvider = ({ children }) => {

    const [maxRoomSizeX, setMaxRoomSizeX] = useState(5)
    const [maxRoomSizeY, setMaxRoomSizeY] = useState(5)
    const [pixelsPerUnit, setPixlesPerUnit] = useState(50)
    const [robotSize, setRobotSize] = useState(32)
    const [showToastReport, setShowToastReport] = useState(false)
    const [animate, setAnimate] = useState(true)

    const value = {
        maxRoomSizeX, setMaxRoomSizeX,
        maxRoomSizeY, setMaxRoomSizeY,
        pixelsPerUnit, setPixlesPerUnit,
        robotSize, setRobotSize,
        animate, setAnimate,
        showToastReport, setShowToastReport
    }

    return (
        <SettingsContext.Provider value={value}>
            {children}
        </SettingsContext.Provider>
    );
}


export const useSettings = () => useContext(SettingsContext)

export default SettingsProvider