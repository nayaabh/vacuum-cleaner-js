import React from 'react'
import './app.less'
import Stage from './components/stage'
import CommandProvider from './context-components/command-provider'
import SettingsProvider from './context-components/settings-provider'

const App = () => {
  return (
    <SettingsProvider>
      <CommandProvider>
        <Stage />
      </CommandProvider>
    </SettingsProvider>
  )
}

export default App