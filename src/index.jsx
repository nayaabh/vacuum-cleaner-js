import React from 'react';
import { render } from 'react-dom';
import App from './app.jsx';

window.React = React;

render(
    <App />,
    document.getElementById('main'),
);