const path = require('path');
const { merge } = require('webpack-merge');
const prodWebpack = require('./webpack.prod.config')

module.exports = merge(prodWebpack, {
  output: {
    path: path.join(__dirname, 'beta'),
    filename: '[name].beta.js',
  },
  devtool: 'source-map',
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'beta'),
    port: 8081,
  },
})