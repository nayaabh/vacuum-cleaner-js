import { getValidNumberWithinRange } from "../src/components/settings-dialog"

describe('Test settings validation Util', () => {
    let MIN_VALUE
    let MAX_VALUE
    beforeAll(() => {
        MIN_VALUE = 1
        MAX_VALUE = 100
    })
    test('Test valid number within range', () => {
        let value = 10
        expect(getValidNumberWithinRange(value, MIN_VALUE, MAX_VALUE)).toBe(10)
    })
    test('Test valid number below min', () => {
        let value = 0
        expect(getValidNumberWithinRange(value, MIN_VALUE, MAX_VALUE)).toBe(1)
    })
    test('Test valid number above min', () => {
        let value = 101
        expect(getValidNumberWithinRange(value, MIN_VALUE, MAX_VALUE)).toBe(100)
    })
    test('Test invalid number', () => {
        let value = "potato"
        expect(getValidNumberWithinRange(value, MIN_VALUE, MAX_VALUE)).toBe(1)
    })
})
