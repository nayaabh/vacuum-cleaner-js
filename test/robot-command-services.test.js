import { EAST, LEFT, MOVE, NORTH, PLACE, REPORT, RIGHT, ROTATE, SOUTH, WEST } from "../src/constants"
import RoboServices, { getRotationDirection, parseCommandString } from "../src/services/robot-command-services"
describe('Test Robot Command Services', () => {
    test('Test robot rotation ', () => {
        expect(getRotationDirection(NORTH, LEFT)).toBe(WEST)
        expect(getRotationDirection(NORTH, RIGHT)).toBe(EAST)
        expect(getRotationDirection(EAST, LEFT)).toBe(NORTH)
        expect(getRotationDirection(EAST, RIGHT)).toBe(SOUTH)
        expect(getRotationDirection(SOUTH, LEFT)).toBe(EAST)
        expect(getRotationDirection(SOUTH, RIGHT)).toBe(WEST)
        expect(getRotationDirection(WEST, LEFT)).toBe(SOUTH)
        expect(getRotationDirection(WEST, RIGHT)).toBe(NORTH)

    })

    test('Test PLACE command parsing ', () => {
        const input = `PLACE 0,0,NORTH
        PLACE 1,1,EAST 
        PLACE 
        PLACE ,,WEST
        PLACE ,1,NORTH_EAST
        PLACE a,1,NORTH
        PLACE 1,x,NORTH
        PLACE 1,x,NORTH
        `
        const parsedCommandList = parseCommandString(input)
        expect(parsedCommandList[0]).toEqual({ commandType: PLACE, params: { positionX: 0, positionY: 0, faceDirection: NORTH } })
        expect(parsedCommandList[1]).toEqual({ commandType: PLACE, params: { positionX: 1, positionY: 1, faceDirection: EAST } })
        expect(parsedCommandList[2]).toBeFalsy()
        expect(parsedCommandList[3]).toBeFalsy()
        expect(parsedCommandList[4]).toBeFalsy()
        expect(parsedCommandList[5]).toBeFalsy()
        expect(parsedCommandList[6]).toBeFalsy()
        expect(parsedCommandList[7]).toBeFalsy()
    })

    test('Test command parsing ', () => {
        const input = `PLACE 0,0,NORTH
        MOVE
        REPORT
        LEFT
        RIGHT
        LALAMOVE
        TOP
        `
        const parsedCommandList = parseCommandString(input)
        expect(parsedCommandList[0]).toEqual({ commandType: PLACE, params: { positionX: 0, positionY: 0, faceDirection: NORTH } })
        expect(parsedCommandList[1]).toEqual({ commandType: MOVE })
        expect(parsedCommandList[2]).toEqual({ commandType: REPORT })
        expect(parsedCommandList[3]).toEqual({ commandType: ROTATE, params: { rotation: LEFT } })
        expect(parsedCommandList[4]).toEqual({ commandType: ROTATE, params: { rotation: RIGHT } })
        expect(parsedCommandList[5]).toBeFalsy()
        expect(parsedCommandList[6]).toBeFalsy()
        expect(parsedCommandList[7]).toBeFalsy()
    })

    test('Test Robot Placement ', () => {
        const robotService = new RoboServices(5, 5)
        let commandList = parseCommandString(`PLACE 0,0,NORTH`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: NORTH })

        commandList = parseCommandString(`PLACE 1,2,EAST`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: EAST })

        // X Index out of bound
        commandList = parseCommandString(`PLACE 5,0,NORTH`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: EAST })

        // Y Index out of bound
        commandList = parseCommandString(`PLACE 0,5,NORTH`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: EAST })

        // Index out of bound
        commandList = parseCommandString(`PLACE 6,7,NORTH`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: EAST })

        // Index out of bound
        commandList = parseCommandString(`PLACE -1,-3,NORTH`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: EAST })

        // Double placement
        commandList = parseCommandString(`PLACE 1,1,SOUTH
        PLACE 1,2,WEST
        `).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 2, faceDirection: WEST })

    })

    test('Test Robot Command before placement ', () => {
        const robotService = new RoboServices(5, 5)
        let commandList = parseCommandString(`MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: null })

        commandList = parseCommandString(`LEFT`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: null })

        commandList = parseCommandString(`RIGHT`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: null })

        commandList = parseCommandString(`REPORT`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: null })

    })

    test('Test Robot Movement', () => {
        const robotService = new RoboServices(5, 5)

        // Middle placement
        let commandList = parseCommandString(`PLACE 1,2,EAST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 2, positionY: 2, faceDirection: EAST })

        // TOP RIGHT
        commandList = parseCommandString(`PLACE 4,4,EAST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 4, positionY: 4, faceDirection: EAST })
        commandList = parseCommandString(`PLACE 4,4,SOUTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 4, positionY: 3, faceDirection: SOUTH })
        commandList = parseCommandString(`PLACE 4,4,WEST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 3, positionY: 4, faceDirection: WEST })

        // BOTTOM LEFT
        commandList = parseCommandString(`PLACE 0,0,WEST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: WEST })
        commandList = parseCommandString(`PLACE 0,0,SOUTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: SOUTH })
        commandList = parseCommandString(`PLACE 0,0,EAST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 1, positionY: 0, faceDirection: EAST })
        commandList = parseCommandString(`PLACE 0,0,NORTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 1, faceDirection: NORTH })

        // MIDDLE Borders
        commandList = parseCommandString(`PLACE 2,4,NORTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 2, positionY: 4, faceDirection: NORTH })
        commandList = parseCommandString(`PLACE 4,2,EAST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 4, positionY: 2, faceDirection: EAST })
        commandList = parseCommandString(`PLACE 2,0,SOUTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 2, positionY: 0, faceDirection: SOUTH })
        commandList = parseCommandString(`PLACE 0,2,WEST
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 2, faceDirection: WEST })

    })

    test('Test Sample TESTs', () => {

        const robotService = new RoboServices(5, 5)

        let commandList = parseCommandString(`PLACE 0,0,NORTH
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 1, faceDirection: NORTH })

        commandList = parseCommandString(`PLACE 0,0,NORTH
        LEFT`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 0, positionY: 0, faceDirection: WEST })

        commandList = parseCommandString(`PLACE 1,2,EAST
        MOVE
        MOVE
        LEFT
        MOVE`).filter(Boolean)
        commandList.forEach(command => robotService.executeCommand(command))
        expect(robotService.robotData).toEqual({ positionX: 3, positionY: 3, faceDirection: NORTH })

    })
})
